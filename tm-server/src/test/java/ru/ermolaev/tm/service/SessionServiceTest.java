package ru.ermolaev.tm.service;

public class SessionServiceTest {

    /*
        Требуется рефакторинг в связи с переходом на новую моель хранения данных
    */

//    final private ISessionRepository sessionRepository = new SessionRepository();
//
//    final private Bootstrap bootstrap = new Bootstrap();
//
//    final private ISessionService sessionService = new SessionService(sessionRepository, bootstrap);
//
//    @Before
//    public void prepareData() throws Exception {
//        bootstrap.getUserService().create("admin","admin","email@test.ru");
//    }
//
//    @After
//    public void removeData() throws Exception {
//        bootstrap.getUserService().removeByLogin("admin");
//    }
//
//    @Test
//    public void checkDataAccessTest() throws Exception {
//        Assert.assertFalse(sessionService.checkDataAccess(null,"admin"));
//        Assert.assertFalse(sessionService.checkDataAccess("admin",null));
//        Assert.assertFalse(sessionService.checkDataAccess("","admin"));
//        Assert.assertFalse(sessionService.checkDataAccess("admin",""));
//        Assert.assertFalse(sessionService.checkDataAccess("admin","wrongPass"));
//        Assert.assertTrue(sessionService.checkDataAccess("admin","admin"));
//    }
//
//    @Test
//    public void isValidTest() throws Exception {
//        final SessionDTO sessionDTO = sessionService.open("admin","admin");
//        Assert.assertNotNull(sessionDTO);
//        Assert.assertTrue(sessionService.isValid(sessionDTO));
//        final SessionDTO newSessionDTO = new SessionDTO();
//        Assert.assertFalse(sessionService.isValid(newSessionDTO));
//    }
//
//    @Test
//    public void openTest() throws Exception {
//        Assert.assertNull(sessionService.open("admin1","admin"));
//        final SessionDTO sessionDTO = sessionService.open("admin","admin");
//        Assert.assertNotNull(sessionDTO);
//        Assert.assertEquals(sessionDTO, sessionRepository.findById(sessionDTO.getId()));
//    }
//
//    @Test
//    public void signTest() {
//        final SessionDTO sessionDTO = new SessionDTO();
//        Assert.assertNotNull(sessionService.sign(sessionDTO));
//        Assert.assertNotNull(sessionDTO.getSignature());
//    }
//
//    @Test
//    public void getUserTest() throws Exception {
//        final UserDTO userDTO = bootstrap.getUserService().findByLogin("admin");
//        final SessionDTO sessionDTO = sessionService.open("admin","admin");
//        Assert.assertNotNull(sessionDTO);
//        Assert.assertEquals(sessionService.getUser(sessionDTO).getId(), userDTO.getId());
//    }
//
//    @Test
//    public void getUserIdTest() throws Exception {
//        final SessionDTO sessionDTO = sessionService.open("admin","admin");
//        Assert.assertNotNull(sessionDTO);
//        final String userId = sessionService.getUserId(sessionDTO);
//        Assert.assertNotNull(userId);
//        Assert.assertEquals(sessionService.getUser(sessionDTO).getId(), userId);
//    }
//
//    @Test
//    public void getSessionListTest() throws Exception {
//        for (int i = 0; i < 5; i++) {
//            sessionService.open("admin", "admin");
//        }
//        final SessionDTO sessionDTO = sessionService.open("admin","admin");
//        final List<SessionDTO> sessionDTOS = sessionService.getSessionList(sessionDTO);
//        Assert.assertNotNull(sessionDTOS);
//        Assert.assertEquals(6, sessionDTOS.size());
//    }
//
//    @Test
//    public void closeTest() throws Exception {
//        final SessionDTO sessionDTO = sessionService.open("admin","admin");
//        final List<SessionDTO> sessionDTOS = sessionService.getSessionList(sessionDTO);
//        Assert.assertEquals(1, sessionDTOS.size());
//        sessionService.close(sessionDTO);
//        Assert.assertEquals(0, sessionService.findAll().size());
//    }
//
//    @Test
//    public void closeAllTest() throws Exception {
//        for (int i = 0; i < 5; i++) {
//            sessionService.open("admin", "admin");
//        }
//        final SessionDTO sessionDTO = sessionService.open("admin","admin");
//        final List<SessionDTO> sessionDTOS = sessionService.getSessionList(sessionDTO);
//        Assert.assertNotNull(sessionDTOS);
//        Assert.assertEquals(6, sessionDTOS.size());
//        sessionService.closeAll(sessionDTO);
//        Assert.assertEquals(0, sessionService.findAll().size());
//    }
//
//    @Test
//    public void signOutByLoginTest() throws Exception {
//        final UserDTO userDTO = bootstrap.getUserService().findByLogin("admin");
//        sessionService.open(userDTO.getLogin(),"admin");
//        Assert.assertEquals(1, sessionService.findAll().size());
//        sessionService.signOutByLogin(userDTO.getLogin());
//        Assert.assertEquals(0, sessionService.findAll().size());
//    }
//
//    @Test
//    public void signOutByUserIdTest() throws Exception {
//        final UserDTO userDTO = bootstrap.getUserService().findByLogin("admin");
//        sessionService.open(userDTO.getLogin(),"admin");
//        Assert.assertEquals(1, sessionService.findAll().size());
//        sessionService.signOutByUserId(userDTO.getId());
//        Assert.assertEquals(0, sessionService.findAll().size());
//    }
//
//    @Test(expected = AccessDeniedException.class)
//    public void validateWithNullSessionTest() throws Exception {
//        sessionService.validate(null);
//    }
//
//    @Test(expected = AccessDeniedException.class)
//    public void validateWithNullSignatureTest() throws Exception {
//        final SessionDTO sessionDTO = sessionService.open("admin","admin");
//        sessionDTO.setSignature(null);
//        sessionService.validate(sessionDTO);
//    }
//
//    @Test(expected = AccessDeniedException.class)
//    public void validateWithNullStartTimeTest() throws Exception {
//        final SessionDTO sessionDTO = sessionService.open("admin","admin");
//        sessionDTO.setStartTime(null);
//        sessionService.validate(sessionDTO);
//    }
//
//    @Test(expected = AccessDeniedException.class)
//    public void validateWithNullUserIdTest() throws Exception {
//        final SessionDTO sessionDTO = sessionService.open("admin","admin");
//        sessionDTO.setUserId(null);
//        sessionService.validate(sessionDTO);
//    }
//
//    @Test(expected = AccessDeniedException.class)
//    public void validateWithRemovedUserTest() throws Exception {
//        final SessionDTO sessionDTO = sessionService.open("admin","admin");
//        bootstrap.getUserService().removeByLogin("admin");
//        sessionService.validate(sessionDTO, Role.USER);
//    }
//
//    @Test(expected = AccessDeniedException.class)
//    public void validateWithIncorrectRoleTest() throws Exception {
//        final SessionDTO sessionDTO = sessionService.open("admin","admin");
//        sessionService.validate(sessionDTO, Role.ADMIN);
//    }
//
//    @Test(expected = AccessDeniedException.class)
//    public void signOutByLoginWithNullLoginTest() throws Exception {
//        sessionService.signOutByLogin(null);
//    }
//
//    @Test(expected = AccessDeniedException.class)
//    public void signOutByLoginWithEmptyLoginTest() throws Exception {
//        sessionService.signOutByLogin("");
//    }
//
//    @Test(expected = AccessDeniedException.class)
//    public void signOutByLoginWithRemovedUserTest() throws Exception {
//        bootstrap.getUserService().removeByLogin("admin");
//        sessionService.signOutByLogin("admin");
//    }
//
//    @Test(expected = AccessDeniedException.class)
//    public void signOutByLoginWithNullUserIdTest() throws Exception {
//        sessionService.signOutByUserId(null);
//    }
//
//    @Test(expected = AccessDeniedException.class)
//    public void signOutByLoginWithEmptyUserIdTest() throws Exception {
//        sessionService.signOutByUserId("");
//    }

}
