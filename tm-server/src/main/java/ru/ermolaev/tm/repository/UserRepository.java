package ru.ermolaev.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.ermolaev.tm.api.repository.IUserRepository;
import ru.ermolaev.tm.entity.User;

import javax.persistence.EntityManager;
import java.util.List;

@Repository
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class UserRepository extends AbstractRepository<User> implements IUserRepository {

    public UserRepository(@NotNull final EntityManager entityManager) {
        super(entityManager);
    }

    @NotNull
    @Override
    public Long count() {
        return entityManager.createQuery("SELECT COUNT(e) FROM User e", Long.class).getSingleResult();
    }

    @Nullable
    @Override
    public User findOneById(@NotNull final String id) {
        return entityManager.find(User.class, id);
    }

    @Nullable
    @Override
    public User findOneByLogin(@NotNull final String login) {
        return entityManager.createQuery("SELECT e FROM User e WHERE e.login = :login", User.class)
                .setParameter("login", login)
                .setMaxResults(1)
                .getSingleResult();
    }

    @NotNull
    @Override
    public List<User> findAll() {
        return entityManager.createQuery("SELECT e FROM User e", User.class).getResultList();
    }

    @Override
    public void removeOneById(@NotNull final String id) {
        @Nullable final User user = findOneById(id);
        if (user == null) return;
        entityManager.remove(user);
    }

    @Override
    public void removeOneByLogin(@NotNull final String login) {
        @Nullable final User user = findOneByLogin(login);
        if (user == null) return;
        entityManager.remove(user);
    }

    @Override
    public void removeAll() {
        @NotNull final List<User> users = findAll();
        for (@NotNull final User user: users) {
            entityManager.remove(user);
        }
    }

}
