package ru.ermolaev.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.ermolaev.tm.api.repository.ISessionRepository;
import ru.ermolaev.tm.entity.Session;

import javax.persistence.EntityManager;
import java.util.List;

@Repository
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class SessionRepository extends AbstractRepository<Session> implements ISessionRepository {

    public SessionRepository(@NotNull final EntityManager entityManager) {
        super(entityManager);
    }

    @NotNull
    @Override
    public Long count() {
        return entityManager.createQuery("SELECT COUNT(e) FROM Session e", Long.class).getSingleResult();
    }

    @NotNull
    @Override
    public Long countByUserId(@NotNull String userId) {
        return entityManager.createQuery("SELECT COUNT(e) FROM Session e WHERE e.user.id = :userId", Long.class)
                .setParameter("userId", userId)
                .getSingleResult();
    }

    @Nullable
    @Override
    public Session findOneById(@NotNull final String id) {
        return entityManager.find(Session.class, id);
    }

    @Nullable
    @Override
    public Session findOneByUserId(@NotNull final String userId) {
        return entityManager.createQuery("SELECT e FROM Session e WHERE e.user.id = :userId", Session.class)
                .setParameter("userId", userId)
                .setMaxResults(1)
                .getSingleResult();
    }

    @NotNull
    @Override
    public List<Session> findAll() {
        return entityManager.createQuery("SELECT e FROM Session e", Session.class).getResultList();
    }

    @NotNull
    @Override
    public List<Session> findAllByUserId(@NotNull final String userId) {
        return entityManager.createQuery("SELECT e FROM Session e WHERE e.user.id = :userId", Session.class)
                .setParameter("userId", userId)
                .getResultList();
    }

    @Override
    public void removeOneById(@NotNull final String id) {
        @Nullable final Session session = findOneById(id);
        if (session == null) return;
        entityManager.remove(session);
    }

    @Override
    public void removeOneByUserId(@NotNull final String userId) {
        @Nullable final Session session = findOneByUserId(userId);
        if (session == null) return;
        entityManager.remove(session);
    }

    @Override
    public void removeAll() {
        @NotNull final List<Session> sessions = findAll();
        for (@NotNull final Session session: sessions) {
            entityManager.remove(session);
        }
    }

    @Override
    public void removeAllByUserId(@NotNull final String userId) {
        @NotNull final List<Session> sessions = findAllByUserId(userId);
        for (@NotNull final Session session: sessions) {
            entityManager.remove(session);
        }
    }

    @Override
    public boolean contains(@NotNull final String id) {
        final Session session = findOneById(id);
        return findAll().contains(session);
    }

}
