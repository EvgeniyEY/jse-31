package ru.ermolaev.tm.bootstrap;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.ermolaev.tm.api.endpoint.*;
import ru.ermolaev.tm.api.service.IPropertyService;

import javax.xml.ws.Endpoint;

@Component
public class Bootstrap {

    private final IPropertyService propertyService;

    private final ITaskEndpoint taskEndpoint;

    private final IProjectEndpoint projectEndpoint;

    private final IUserEndpoint userEndpoint;

    private final ISessionEndpoint sessionEndpoint;

    private final IAdminUserEndpoint adminUserEndpoint;

    private final IAdminDataEndpoint adminDataEndpoint;

    @Autowired
    public Bootstrap(
            @NotNull final IPropertyService propertyService,
            @NotNull final ITaskEndpoint taskEndpoint,
            @NotNull final IProjectEndpoint projectEndpoint,
            @NotNull final IUserEndpoint userEndpoint,
            @NotNull final ISessionEndpoint sessionEndpoint,
            @NotNull final IAdminUserEndpoint adminUserEndpoint,
            @NotNull final IAdminDataEndpoint adminDataEndpoint
    ) {
        this.propertyService = propertyService;
        this.taskEndpoint = taskEndpoint;
        this.projectEndpoint = projectEndpoint;
        this.userEndpoint = userEndpoint;
        this.sessionEndpoint = sessionEndpoint;
        this.adminUserEndpoint = adminUserEndpoint;
        this.adminDataEndpoint = adminDataEndpoint;
    }

    private void initEndpoint() {
        final String url = "http://" + propertyService.getServerHost() + ":" + propertyService.getServerPort();

        final String sessionEndpointUrl = url + "/SessionEndpoint?wsdl";
        Endpoint.publish(sessionEndpointUrl, sessionEndpoint);
        System.out.println(sessionEndpointUrl);

        final String taskEndpointUrl = url + "/TaskEndpoint?wsdl";
        Endpoint.publish(taskEndpointUrl, taskEndpoint);
        System.out.println(taskEndpointUrl);

        final String projectEndpointUrl = url + "/ProjectEndpoint?wsdl";
        Endpoint.publish(projectEndpointUrl, projectEndpoint);
        System.out.println(projectEndpointUrl);

        final String userEndpointUrl = url + "/UserEndpoint?wsdl";
        Endpoint.publish(userEndpointUrl, userEndpoint);
        System.out.println(userEndpointUrl);

        final String adminUserEndpointUrl = url + "/AdminEndpoint?wsdl";
        Endpoint.publish(adminUserEndpointUrl, adminUserEndpoint);
        System.out.println(adminUserEndpointUrl);

        final String adminDataEndpointUrl = url + "/AdminDataEndpoint?wsdl";
        Endpoint.publish(adminDataEndpointUrl, adminDataEndpoint);
        System.out.println(adminDataEndpointUrl);
    }

    @SneakyThrows
    public void run() {
        System.out.println("Starting a task-manager server");
        initEndpoint();
    }

}
