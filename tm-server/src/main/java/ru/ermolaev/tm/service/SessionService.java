package ru.ermolaev.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.ermolaev.tm.api.repository.ISessionRepository;
import ru.ermolaev.tm.dto.SessionDTO;
import ru.ermolaev.tm.dto.UserDTO;
import ru.ermolaev.tm.entity.Session;
import ru.ermolaev.tm.entity.User;
import ru.ermolaev.tm.enumeration.Role;
import ru.ermolaev.tm.exception.user.AccessDeniedException;
import ru.ermolaev.tm.repository.SessionRepository;
import ru.ermolaev.tm.util.HashUtil;
import ru.ermolaev.tm.util.SignatureUtil;
import ru.ermolaev.tm.api.service.IDatabaseConnectionService;
import ru.ermolaev.tm.api.service.IPropertyService;
import ru.ermolaev.tm.api.service.ISessionService;
import ru.ermolaev.tm.api.service.IUserService;

import javax.persistence.EntityManager;
import java.util.List;

@Service
public class SessionService extends AbstractService<Session> implements ISessionService {

    private final IUserService userService;

    private final IPropertyService propertyService;

    @Autowired
    public SessionService(
            @NotNull final IDatabaseConnectionService databaseConnectionService,
            @NotNull final IUserService userService,
            @NotNull final IPropertyService propertyService
    ) {
        super(databaseConnectionService);
        this.userService = userService;
        this.propertyService = propertyService;
    }

    @Override
    public void persist(@Nullable final Session session) {
        if (session == null) return;
        @NotNull final EntityManager entityManager = databaseConnectionService.getEntityManager();
        entityManager.getTransaction().begin();
        @NotNull final ISessionRepository repository = new SessionRepository(entityManager);
        try {
            repository.persist(session);
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void merge(@Nullable final Session session) {
        if (session == null) return;
        @NotNull final EntityManager entityManager = databaseConnectionService.getEntityManager();
        entityManager.getTransaction().begin();
        @NotNull final ISessionRepository repository = new SessionRepository(entityManager);
        try {
            repository.merge(session);
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void remove(@Nullable final Session session) {
        if (session == null) return;
        @NotNull final EntityManager entityManager = databaseConnectionService.getEntityManager();
        entityManager.getTransaction().begin();
        @NotNull final ISessionRepository repository = new SessionRepository(entityManager);
        try {
            repository.remove(session);
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public boolean checkDataAccess(@Nullable final String login, @Nullable final String password) throws Exception {
        if (login == null || login.isEmpty()) return false;
        if (password == null || password.isEmpty()) return false;
        @Nullable final UserDTO userDTO = UserDTO.toDTO(userService.findOneByLogin(login));
        if (userDTO == null) return false;
        @Nullable final String passwordHash = HashUtil.hidePassword(password);
        if (passwordHash == null || passwordHash.isEmpty()) return false;
        return passwordHash.equals(userDTO.getPasswordHash());
    }

    @Override
    public boolean isValid(@Nullable final SessionDTO sessionDTO) {
        try {
            validate(sessionDTO);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    @Override
    public void validate(@Nullable final SessionDTO sessionDTO) throws Exception {
        if (sessionDTO == null) throw new AccessDeniedException();
        if (sessionDTO.getSignature() == null) throw new AccessDeniedException();
        if (sessionDTO.getStartTime() == null) throw new AccessDeniedException();
        if (sessionDTO.getUserId() == null) throw new AccessDeniedException();
        @Nullable final SessionDTO temp = sessionDTO.clone();
        if (temp == null) throw new AccessDeniedException();
        @Nullable final String signatureSource = sessionDTO.getSignature();
        @Nullable final String signatureTarget = createSignature(temp);
        final boolean checkSignature = signatureSource.equals(signatureTarget);
        if (!checkSignature) throw new AccessDeniedException();
        @NotNull final EntityManager entityManager = databaseConnectionService.getEntityManager();
        @NotNull final ISessionRepository repository = new SessionRepository(entityManager);
        final boolean isContain = repository.contains(sessionDTO.getId());
        entityManager.close();
        if (!isContain) throw new AccessDeniedException();
    }

    @Override
    public void validate(@Nullable final SessionDTO sessionDTO, @Nullable final Role role) throws Exception {
        if (role == null) return;
        validate(sessionDTO);
        @NotNull final String userId = sessionDTO.getUserId();
        @Nullable final UserDTO userDTO = UserDTO.toDTO(userService.findOneById(userId));
        if (userDTO == null) throw new AccessDeniedException();
        if (!role.equals(userDTO.getRole())) throw new AccessDeniedException();
    }

    @Nullable
    @Override
    public SessionDTO open(@Nullable final String login, @Nullable final String password) throws Exception {
        final boolean check = checkDataAccess(login, password);
        if (!check) return null;
        @Nullable final User user = userService.findOneByLogin(login);
        if (user == null) return null;
        @NotNull final Session session = new Session();
        session.setUser(user);
        session.setStartTime(System.currentTimeMillis());
        session.setSignature(createSignature(SessionDTO.toDTO(session)));
        persist(session);
        return SessionDTO.toDTO(session);
    }

    @Nullable
    @Override
    public String createSignature(@Nullable final SessionDTO sessionDTO) {
        if (sessionDTO == null) return null;
        sessionDTO.setSignature(null);
        @Nullable final String salt = propertyService.getSessionSalt();
        @Nullable final Integer cycle = propertyService.getSessionCycle();
        return SignatureUtil.sign(sessionDTO, salt, cycle);
    }

    @Nullable
    @Override
    public UserDTO getUser(@Nullable final SessionDTO sessionDTO) throws Exception {
        @Nullable final String userId = getUserId(sessionDTO);
        if (userId == null) return null;
        return UserDTO.toDTO(userService.findOneById(userId));
    }

    @Nullable
    @Override
    public String getUserId(@Nullable final SessionDTO sessionDTO) throws Exception {
        validate(sessionDTO);
        return sessionDTO.getUserId();
    }

    @NotNull
    @Override
    public List<SessionDTO> getSessionList(@Nullable final SessionDTO sessionDTO) throws Exception {
        validate(sessionDTO);
        @NotNull final EntityManager entityManager = databaseConnectionService.getEntityManager();
        @NotNull final ISessionRepository repository = new SessionRepository(entityManager);
        @NotNull final List<SessionDTO> sessionsDTO = SessionDTO.toDTO(repository.findAllByUserId(sessionDTO.getUserId()));
        entityManager.close();
        return sessionsDTO;
    }

    @Override
    public void close(@Nullable final SessionDTO sessionDTO) throws Exception {
        validate(sessionDTO);
        @NotNull final EntityManager entityManager = databaseConnectionService.getEntityManager();
        entityManager.getTransaction().begin();
        @NotNull final ISessionRepository repository = new SessionRepository(entityManager);
        try {
            repository.removeOneById(sessionDTO.getId());
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void closeAll(@Nullable final SessionDTO sessionDTO) throws Exception {
        validate(sessionDTO);
        @NotNull final EntityManager entityManager = databaseConnectionService.getEntityManager();
        entityManager.getTransaction().begin();
        @NotNull final ISessionRepository repository = new SessionRepository(entityManager);
        try {
            repository.removeAllByUserId(sessionDTO.getUserId());
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void signOutByLogin(@Nullable final String login) throws Exception {
        if (login == null || login.isEmpty()) throw new AccessDeniedException();
        @Nullable final UserDTO userDTO = UserDTO.toDTO(userService.findOneByLogin(login));
        if (userDTO == null) throw new AccessDeniedException();
        @NotNull final String userId = userDTO.getId();
        @NotNull final EntityManager entityManager = databaseConnectionService.getEntityManager();
        entityManager.getTransaction().begin();
        @NotNull final ISessionRepository repository = new SessionRepository(entityManager);
        try {
            repository.removeOneByUserId(userId);
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
        } finally {
           entityManager.close();
        }
    }

    @Override
    public void signOutByUserId(@Nullable final String userId) throws Exception {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        @NotNull final EntityManager entityManager = databaseConnectionService.getEntityManager();
        entityManager.getTransaction().begin();
        @NotNull final ISessionRepository repository = new SessionRepository(entityManager);
        try {
            repository.removeOneByUserId(userId);
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
        } finally {
            entityManager.close();
        }
    }

}
