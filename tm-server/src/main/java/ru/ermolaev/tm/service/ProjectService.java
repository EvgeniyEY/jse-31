package ru.ermolaev.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.ermolaev.tm.api.repository.IProjectRepository;
import ru.ermolaev.tm.api.service.IProjectService;
import ru.ermolaev.tm.api.service.IDatabaseConnectionService;
import ru.ermolaev.tm.api.service.IUserService;
import ru.ermolaev.tm.dto.ProjectDTO;
import ru.ermolaev.tm.entity.Project;
import ru.ermolaev.tm.exception.incorrect.IncorrectCompleteDateException;
import ru.ermolaev.tm.exception.incorrect.IncorrectStartDateException;
import ru.ermolaev.tm.repository.ProjectRepository;
import ru.ermolaev.tm.exception.empty.*;

import javax.persistence.EntityManager;
import java.util.Date;
import java.util.List;

@Service
public class ProjectService extends AbstractService<Project> implements IProjectService {

    private final IUserService userService;

    @Autowired
    public ProjectService(
            @NotNull final IDatabaseConnectionService databaseConnectionService,
            @NotNull final IUserService userService
    ) {
        super(databaseConnectionService);
        this.userService = userService;
    }

    @Override
    public void persist(@Nullable final Project project) {
        if (project == null) return;
        @NotNull final EntityManager entityManager = databaseConnectionService.getEntityManager();
        entityManager.getTransaction().begin();
        @NotNull final IProjectRepository repository = new ProjectRepository(entityManager);
        try {
            repository.persist(project);
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void merge(@Nullable final Project project) {
        if (project == null) return;
        @NotNull final EntityManager entityManager = databaseConnectionService.getEntityManager();
        entityManager.getTransaction().begin();
        @NotNull final IProjectRepository repository = new ProjectRepository(entityManager);
        try {
            repository.merge(project);
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void remove(@Nullable final Project project) {
        if (project == null) return;
        @NotNull final EntityManager entityManager = databaseConnectionService.getEntityManager();
        entityManager.getTransaction().begin();
        @NotNull final IProjectRepository repository = new ProjectRepository(entityManager);
        try {
            repository.remove(project);
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void createProject(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description
    ) throws Exception {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (description == null || description.isEmpty()) throw new EmptyDescriptionException();
        @NotNull final Project project = new Project();
        project.setName(name);
        project.setDescription(description);
        project.setUser(userService.findOneById(userId));
        persist(project);
    }

    @Override
    public void updateById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) throws Exception {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (name == null) throw new EmptyNameException();
        if (description == null) throw new EmptyDescriptionException();
        @NotNull final Project project = findOneById(userId, id);
        if (!name.isEmpty()) project.setName(name);
        if (!description.isEmpty()) project.setDescription(description);
        if (name.isEmpty() && description.isEmpty()) return;
        merge(project);
    }

    @Override
    public void updateStartDate(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final Date date
    ) throws Exception {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (date == null) throw new EmptyDateException();
        @NotNull final Project project = findOneById(userId, id);
        if (date.before(new Date(System.currentTimeMillis()))) throw new IncorrectStartDateException(date);
        project.setStartDate(date);
        merge(project);
    }

    @Override
    public void updateCompleteDate(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final Date date
    ) throws Exception {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (date == null) throw new EmptyDateException();
        @NotNull final Project project = findOneById(userId, id);
        if (project.getStartDate() == null) throw new IncorrectCompleteDateException(date);
        if (project.getStartDate().after(date)) throw new IncorrectCompleteDateException(date);
        project.setCompleteDate(date);
        merge(project);
    }

    @NotNull
    @Override
    public Long countAllProjects() {
        @NotNull final EntityManager entityManager = databaseConnectionService.getEntityManager();
        @NotNull final IProjectRepository repository = new ProjectRepository(entityManager);
        @NotNull final Long countOfProjects = repository.count();
        entityManager.close();
        return countOfProjects;
    }

    @NotNull
    @Override
    public Long countUserProjects(@Nullable final String userId) throws Exception {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        @NotNull final EntityManager entityManager = databaseConnectionService.getEntityManager();
        @NotNull final IProjectRepository repository = new ProjectRepository(entityManager);
        @NotNull final Long countOfProjects = repository.countByUserId(userId);
        entityManager.close();
        return countOfProjects;
    }

    @NotNull
    @Override
    public Project findOneById(@Nullable final String userId, @Nullable final String id) throws Exception {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @NotNull final EntityManager entityManager = databaseConnectionService.getEntityManager();
        @NotNull final IProjectRepository repository = new ProjectRepository(entityManager);
        @NotNull final Project project = repository.findOneById(userId, id);
        entityManager.close();
        return project;
    }

    @NotNull
    @Override
    public Project findOneByName(@Nullable final String userId, @Nullable final String name) throws Exception {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NotNull final EntityManager entityManager = databaseConnectionService.getEntityManager();
        @NotNull final IProjectRepository repository = new ProjectRepository(entityManager);
        @NotNull final Project project = repository.findOneByName(userId, name);
        entityManager.close();
        return project;
    }

    @NotNull
    @Override
    public List<ProjectDTO> findAll() {
        @NotNull final EntityManager entityManager = databaseConnectionService.getEntityManager();
        @NotNull final IProjectRepository repository = new ProjectRepository(entityManager);
        @NotNull final List<ProjectDTO> projectsDTO = ProjectDTO.toDTO(repository.findAll());
        entityManager.close();
        return projectsDTO;
    }

    @NotNull
    @Override
    public List<ProjectDTO> findAllByUserId(@Nullable final String userId) throws Exception {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        @NotNull final EntityManager entityManager = databaseConnectionService.getEntityManager();
        @NotNull final IProjectRepository repository = new ProjectRepository(entityManager);
        @NotNull final List<ProjectDTO> projectsDTO = ProjectDTO.toDTO(repository.findAllByUserId(userId));
        entityManager.close();
        return projectsDTO;
    }

    @Override
    public void removeOneById(@Nullable final String userId, @Nullable final String id) throws Exception {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @NotNull final EntityManager entityManager = databaseConnectionService.getEntityManager();
        entityManager.getTransaction().begin();
        @NotNull final IProjectRepository repository = new ProjectRepository(entityManager);
        try {
            repository.removeOneById(userId, id);
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void removeOneByName(@Nullable final String userId, @Nullable final String name) throws Exception {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NotNull final EntityManager entityManager = databaseConnectionService.getEntityManager();
        entityManager.getTransaction().begin();
        @NotNull final IProjectRepository repository = new ProjectRepository(entityManager);
        try {
            repository.removeOneByName(userId, name);
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void removeAll() {
        @NotNull final EntityManager entityManager = databaseConnectionService.getEntityManager();
        entityManager.getTransaction().begin();
        @NotNull final IProjectRepository repository = new ProjectRepository(entityManager);
        try {
            repository.removeAll();
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void removeAllByUserId(@Nullable final String userId) throws Exception {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        @NotNull final EntityManager entityManager = databaseConnectionService.getEntityManager();
        entityManager.getTransaction().begin();
        @NotNull final IProjectRepository repository = new ProjectRepository(entityManager);
        try {
            repository.removeAllByUserId(userId);
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
        } finally {
            entityManager.close();
        }
    }

}
