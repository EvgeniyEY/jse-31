package ru.ermolaev.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ermolaev.tm.entity.Session;

import java.util.List;

public interface ISessionRepository extends IRepository<Session> {

    @NotNull
    Long count();

    @NotNull
    Long countByUserId(@NotNull String userId);

    @Nullable
    Session findOneById(@NotNull String id);

    @Nullable
    Session findOneByUserId(@NotNull String userId);

    @NotNull
    List<Session> findAll();

    @NotNull
    List<Session> findAllByUserId(@NotNull String userId);

    void removeOneById(@NotNull String id);

    void removeOneByUserId(@NotNull String userId);

    void removeAll();

    void removeAllByUserId(@NotNull String userId);

    boolean contains(@NotNull String id);

}
