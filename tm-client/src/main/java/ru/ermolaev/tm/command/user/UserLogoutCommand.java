package ru.ermolaev.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.ermolaev.tm.command.AbstractCommand;
import ru.ermolaev.tm.endpoint.SessionDTO;
import ru.ermolaev.tm.endpoint.SessionEndpoint;
import ru.ermolaev.tm.service.SessionService;

@Component
public class UserLogoutCommand extends AbstractCommand {

    private final SessionEndpoint sessionEndpoint;

    @NotNull
    @Override
    public String commandName() {
        return "logout";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Logout from your account.";
    }

    @Autowired
    public UserLogoutCommand(
            @NotNull final SessionEndpoint sessionEndpoint,
            @NotNull final SessionService sessionService
    ) {
        super(sessionService);
        this.sessionEndpoint = sessionEndpoint;
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[LOGOUT]");
        @Nullable final SessionDTO session = sessionService.getCurrentSession();
        sessionEndpoint.closeSession(session);
        sessionService.clearCurrentSession();
        System.out.println("[OK]");
    }

}
