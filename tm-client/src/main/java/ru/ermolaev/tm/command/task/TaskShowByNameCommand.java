package ru.ermolaev.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.ermolaev.tm.api.ISessionService;
import ru.ermolaev.tm.endpoint.SessionDTO;
import ru.ermolaev.tm.endpoint.TaskDTO;
import ru.ermolaev.tm.endpoint.TaskEndpoint;
import ru.ermolaev.tm.util.TerminalUtil;

@Component
public class TaskShowByNameCommand extends AbstractTaskCommand {

    @NotNull
    @Override
    public String commandName() {
        return "task-show-by-name";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Show task by name.";
    }

    @Autowired
    public TaskShowByNameCommand(
            @NotNull final TaskEndpoint taskEndpoint,
            @NotNull final ISessionService sessionService
    ) {
        super(taskEndpoint, sessionService);
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[SHOW TASK]");
        System.out.println("ENTER TASK NAME:");
        @Nullable final String name = TerminalUtil.nextLine();
        @Nullable final SessionDTO session = sessionService.getCurrentSession();
        @NotNull final TaskDTO task = taskEndpoint.findTaskByName(session, name);
        System.out.println("ID: " + task.getId());
        System.out.println("NAME: " + task.getName());
        System.out.println("DESCRIPTION: " + task.getDescription());
        System.out.println("[COMPLETE]");
    }

}
