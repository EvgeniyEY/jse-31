package ru.ermolaev.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.ermolaev.tm.command.AbstractCommand;

@Component
public class ExitCommand extends AbstractCommand {

    @NotNull
    @Override
    public String commandName() {
        return "exit";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Exit from application.";
    }

    @Override
    public void execute() {
        System.exit(0);
    }

}
