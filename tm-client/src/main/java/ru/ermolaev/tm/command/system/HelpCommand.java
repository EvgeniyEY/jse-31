package ru.ermolaev.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.ermolaev.tm.bootstrap.Bootstrap;
import ru.ermolaev.tm.command.AbstractCommand;

import java.util.List;

@Component
public class HelpCommand extends AbstractCommand {

    private final Bootstrap bootstrap;

    @NotNull
    @Override
    public String commandName() {
        return "help";
    }

    @NotNull
    @Override
    public String arg() {
        return "-h";
    }

    @NotNull
    @Override
    public String description() {
        return "Display terminal commands.";
    }

    @Autowired
    public HelpCommand(@NotNull final Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
    }

    @Override
    public void execute() {
        System.out.println("[HELP]");
        @NotNull final List<AbstractCommand> commands = bootstrap.getCommandList();
        for (@NotNull final AbstractCommand command: commands) {
            System.out.println(command.commandName()
                    + (command.arg() == null ? ": " : ", " + command.arg() + ": ")
                    + command.description());
        }
    }

}
