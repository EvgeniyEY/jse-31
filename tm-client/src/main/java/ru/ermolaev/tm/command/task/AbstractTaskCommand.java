package ru.ermolaev.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.ermolaev.tm.api.ISessionService;
import ru.ermolaev.tm.command.AbstractCommand;
import ru.ermolaev.tm.endpoint.TaskEndpoint;

@Component
public abstract class AbstractTaskCommand extends AbstractCommand {

    protected TaskEndpoint taskEndpoint;

    @Autowired
    public AbstractTaskCommand(
            @NotNull final TaskEndpoint taskEndpoint,
            @NotNull final ISessionService sessionService
    ) {
        super(sessionService);
        this.taskEndpoint = taskEndpoint;
    }

}
