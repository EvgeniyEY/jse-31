package ru.ermolaev.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.ermolaev.tm.api.ISessionService;
import ru.ermolaev.tm.endpoint.SessionDTO;
import ru.ermolaev.tm.endpoint.TaskDTO;
import ru.ermolaev.tm.endpoint.TaskEndpoint;

import java.util.List;

@Component
public class TaskListShowCommand extends AbstractTaskCommand {

    @NotNull
    @Override
    public String commandName() {
        return "task-list";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Show task list.";
    }

    @Autowired
    public TaskListShowCommand(
            @NotNull final TaskEndpoint taskEndpoint,
            @NotNull final ISessionService sessionService
    ) {
        super(taskEndpoint, sessionService);
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[TASK LIST]");
        @Nullable final SessionDTO session = sessionService.getCurrentSession();
        @Nullable final List<TaskDTO> tasks = taskEndpoint.findAllTasks(session);
        if (tasks == null) return;
        for (@NotNull final TaskDTO task: tasks) {
            System.out.println((tasks.indexOf(task) + 1)
                    + ". {id: "
                    + task.getId()
                    + "; name: "
                    + task.getName()
                    + "; description: "
                    + task.getDescription()
                    + "}");
        }
        System.out.println("[COMPLETE]");
    }

}
