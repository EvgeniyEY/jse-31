package ru.ermolaev.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.ermolaev.tm.api.ISessionService;
import ru.ermolaev.tm.endpoint.ProjectDTO;
import ru.ermolaev.tm.endpoint.ProjectEndpoint;
import ru.ermolaev.tm.endpoint.SessionDTO;

import java.util.List;

@Component
public class ProjectListShowCommand extends AbstractProjectCommand {

    @NotNull
    @Override
    public String commandName() {
        return "project-list";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Show project list.";
    }

    @Autowired
    public ProjectListShowCommand(
            @NotNull final ProjectEndpoint projectEndpoint,
            @NotNull final ISessionService sessionService
    ) {
        super(projectEndpoint, sessionService);
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[PROJECTS LIST]");
        @Nullable final SessionDTO session = sessionService.getCurrentSession();
        @NotNull final List<ProjectDTO> projects = projectEndpoint.findAllProjects(session);
        if (projects == null) return;
        for (@NotNull final ProjectDTO project: projects) {
            System.out.println((projects.indexOf(project) + 1)
                    + ". {id: "
                    + project.getId()
                    + "; name: "
                    + project.getName()
                    + "; description: "
                    + project.getDescription()
                    + "}");
        }
        System.out.println("[COMPLETE]");
    }

}
