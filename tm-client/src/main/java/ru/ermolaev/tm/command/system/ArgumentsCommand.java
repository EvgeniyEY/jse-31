package ru.ermolaev.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.ermolaev.tm.bootstrap.Bootstrap;
import ru.ermolaev.tm.command.AbstractCommand;

import java.util.List;

@Component
public class ArgumentsCommand extends AbstractCommand {

    private final Bootstrap bootstrap;

    @NotNull
    @Override
    public String commandName() {
        return "arguments";
    }

    @NotNull
    @Override
    public String arg() {
        return "-arg";
    }

    @NotNull
    @Override
    public String description() {
        return "Show application's arguments.";
    }

    @Autowired
    public ArgumentsCommand(@NotNull final Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
    }

    @Override
    public void execute() {
        @NotNull final List<AbstractCommand> commands = bootstrap.getCommandList();
        for (@NotNull final AbstractCommand command: commands) {
            if (command.arg() == null) continue;
            System.out.println(command.arg());
        }
    }

}
