package ru.ermolaev.tm.command.data.base64;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.ermolaev.tm.api.ISessionService;
import ru.ermolaev.tm.command.data.AbstractDataCommand;
import ru.ermolaev.tm.endpoint.AdminDataEndpoint;
import ru.ermolaev.tm.endpoint.SessionDTO;

@Component
public class DataBase64SaveCommand extends AbstractDataCommand {

    @NotNull
    @Override
    public String commandName() {
        return "data-base64-save";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Save data to base64 file.";
    }

    @Autowired
    public DataBase64SaveCommand(
            @NotNull final AdminDataEndpoint adminDataEndpoint,
            @NotNull final ISessionService sessionService
    ) {
        super(adminDataEndpoint, sessionService);
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[DATA BASE64 SAVE]");
        @Nullable final SessionDTO session = sessionService.getCurrentSession();
        adminDataEndpoint.saveBase64(session);
        System.out.println("[OK]");
    }

}
