package ru.ermolaev.tm.command.data.xml.jaxb;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.ermolaev.tm.api.ISessionService;
import ru.ermolaev.tm.command.data.AbstractDataCommand;
import ru.ermolaev.tm.endpoint.AdminDataEndpoint;
import ru.ermolaev.tm.endpoint.SessionDTO;

@Component
public class DataXmlJaxbSaveCommand extends AbstractDataCommand {

    @NotNull
    @Override
    public String commandName() {
        return "data-xml-jb-save";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Save data to XML (Jax-B) file.";
    }

    @Autowired
    public DataXmlJaxbSaveCommand(
            @NotNull final AdminDataEndpoint adminDataEndpoint,
            @NotNull final ISessionService sessionService
    ) {
        super(adminDataEndpoint, sessionService);
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[DATA XML (JAX-B) SAVE]");
        @Nullable final SessionDTO session = sessionService.getCurrentSession();
        adminDataEndpoint.saveXmlByJaxb(session);
        System.out.println("[OK]");
    }

}
