package ru.ermolaev.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.ermolaev.tm.api.ISessionService;
import ru.ermolaev.tm.endpoint.SessionDTO;
import ru.ermolaev.tm.endpoint.UserEndpoint;
import ru.ermolaev.tm.util.TerminalUtil;

@Component
public class UserFirstNameUpdateCommand extends AbstractUserCommand {

    @NotNull
    @Override
    public String commandName() {
        return "user-update-first-name";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Update user first name.";
    }

    @Autowired
    public UserFirstNameUpdateCommand(
            @NotNull final UserEndpoint userEndpoint,
            @NotNull final ISessionService sessionService
    ) {
        super(userEndpoint, sessionService);
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[UPDATE USER FIRST NAME]");
        System.out.println("ENTER NEW USER FIRST NAME:");
        @Nullable final String newFirstName = TerminalUtil.nextLine();
        @Nullable final SessionDTO session = sessionService.getCurrentSession();
        userEndpoint.updateUserFirstName(session, newFirstName);
        System.out.println("[OK]");
    }

}
