package ru.ermolaev.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.ermolaev.tm.command.AbstractCommand;
import ru.ermolaev.tm.util.NumberUtil;

@Component
public class SystemInfoCommand extends AbstractCommand {

    @NotNull
    @Override
    public String commandName() {
        return "info";
    }

    @NotNull
    @Override
    public String arg() {
        return "-i";
    }

    @NotNull
    @Override
    public String description() {
        return "Show hardware info.";
    }

    @Override
    public void execute() {
        System.out.println("Available processors (cores): " + Runtime.getRuntime().availableProcessors());
        System.out.println("Free memory: " + NumberUtil.formatBytes(Runtime.getRuntime().freeMemory()));
        System.out.println("Maximum memory: " + (Runtime.getRuntime().maxMemory() == Long.MAX_VALUE ? "no limit" : NumberUtil.formatBytes(Runtime.getRuntime().maxMemory())));
        System.out.println("Total memory available to JVM: " + NumberUtil.formatBytes(Runtime.getRuntime().totalMemory()));
        System.out.println("Used memory by JVM: " + NumberUtil.formatBytes(Runtime.getRuntime().totalMemory()-Runtime.getRuntime().freeMemory()));
    }

}
