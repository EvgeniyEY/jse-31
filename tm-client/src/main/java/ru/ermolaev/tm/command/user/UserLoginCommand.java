package ru.ermolaev.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.ermolaev.tm.command.AbstractCommand;
import ru.ermolaev.tm.endpoint.SessionDTO;
import ru.ermolaev.tm.endpoint.SessionEndpoint;
import ru.ermolaev.tm.service.SessionService;
import ru.ermolaev.tm.util.TerminalUtil;

@Component
public class UserLoginCommand extends AbstractCommand {

    private final SessionEndpoint sessionEndpoint;

    @NotNull
    @Override
    public String commandName() {
        return "login";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Login in your account.";
    }

    @Autowired
    public UserLoginCommand(
            @NotNull final SessionEndpoint sessionEndpoint,
            @NotNull final SessionService sessionService
    ) {
        super(sessionService);
        this.sessionEndpoint = sessionEndpoint;
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[LOGIN]");
        System.out.println("ENTER LOGIN:");
        @Nullable final String login = TerminalUtil.nextLine();
        System.out.println("ENTER PASSWORD:");
        @Nullable final String password = TerminalUtil.nextLine();
        @Nullable final SessionDTO session = sessionEndpoint.openSession(login, password);
        sessionService.setCurrentSession(session);
        System.out.println("[OK]");
    }

}
