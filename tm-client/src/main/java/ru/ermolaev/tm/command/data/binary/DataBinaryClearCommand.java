package ru.ermolaev.tm.command.data.binary;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.ermolaev.tm.api.ISessionService;
import ru.ermolaev.tm.command.data.AbstractDataCommand;
import ru.ermolaev.tm.endpoint.AdminDataEndpoint;
import ru.ermolaev.tm.endpoint.SessionDTO;

@Component
public class DataBinaryClearCommand extends AbstractDataCommand {

    @NotNull
    @Override
    public String commandName() {
        return "data-bin-clear";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Remove binary file.";
    }

    @Autowired
    public DataBinaryClearCommand(
            @NotNull final AdminDataEndpoint adminDataEndpoint,
            @NotNull final ISessionService sessionService
    ) {
        super(adminDataEndpoint, sessionService);
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[REMOVE BINARY FILE]");
        @Nullable final SessionDTO session = sessionService.getCurrentSession();
        adminDataEndpoint.clearBinaryFile(session);
        System.out.println("[OK]");
    }

}
