package ru.ermolaev.tm.command.data.base64;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.ermolaev.tm.api.ISessionService;
import ru.ermolaev.tm.command.data.AbstractDataCommand;
import ru.ermolaev.tm.endpoint.AdminDataEndpoint;
import ru.ermolaev.tm.endpoint.SessionDTO;

@Component
public class DataBase64LoadCommand extends AbstractDataCommand {

    @NotNull
    @Override
    public String commandName() {
        return "data-base64-load";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Load data from base64 file.";
    }

    @Autowired
    public DataBase64LoadCommand(
            @NotNull final AdminDataEndpoint adminDataEndpoint,
            @NotNull final ISessionService sessionService
    ) {
        super(adminDataEndpoint, sessionService);
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[DATA BASE64 LOAD]");
        @Nullable final SessionDTO session = sessionService.getCurrentSession();
        adminDataEndpoint.loadBase64(session);
        System.out.println("[OK]");
    }

}
