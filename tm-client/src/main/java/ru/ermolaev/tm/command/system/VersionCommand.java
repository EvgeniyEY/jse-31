package ru.ermolaev.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.ermolaev.tm.command.AbstractCommand;

@Component
public class VersionCommand extends AbstractCommand {

    @NotNull
    @Override
    public String commandName() {
        return "version";
    }

    @NotNull
    @Override
    public String arg() {
        return "-v";
    }

    @NotNull
    @Override
    public String description() {
        return "Show version info.";
    }

    @Override
    public void execute() {
        System.out.println("[VERSION]");
        System.out.println("1.0.22");
    }

}
