package ru.ermolaev.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.ermolaev.tm.api.ISessionService;
import ru.ermolaev.tm.endpoint.ProjectDTO;
import ru.ermolaev.tm.endpoint.ProjectEndpoint;
import ru.ermolaev.tm.endpoint.SessionDTO;
import ru.ermolaev.tm.util.TerminalUtil;

@Component
public class ProjectShowByIdCommand extends AbstractProjectCommand {

    @NotNull
    @Override
    public String commandName() {
        return "project-show-by-id";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Show project by id.";
    }

    @Autowired
    public ProjectShowByIdCommand(
            @NotNull final ProjectEndpoint projectEndpoint,
            @NotNull final ISessionService sessionService
    ) {
        super(projectEndpoint, sessionService);
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[SHOW PROJECT]");
        System.out.println("ENTER PROJECT ID:");
        @Nullable final String id = TerminalUtil.nextLine();
        @Nullable final SessionDTO session = sessionService.getCurrentSession();
        @NotNull final ProjectDTO project = projectEndpoint.findProjectById(session, id);
        System.out.println("ID: " + project.getId());
        System.out.println("NAME: " + project.getName());
        System.out.println("DESCRIPTION: " + project.getDescription());
        System.out.println("[COMPLETE]");
    }

}
