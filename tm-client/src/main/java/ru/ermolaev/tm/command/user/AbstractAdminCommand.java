package ru.ermolaev.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.ermolaev.tm.api.ISessionService;
import ru.ermolaev.tm.command.AbstractCommand;
import ru.ermolaev.tm.endpoint.AdminUserEndpoint;

@Component
public abstract class AbstractAdminCommand extends AbstractCommand {

    protected AdminUserEndpoint adminUserEndpoint;

    @Autowired
    public AbstractAdminCommand(
            @NotNull final AdminUserEndpoint adminUserEndpoint,
            @NotNull final ISessionService sessionService
    ) {
        super(sessionService);
        this.adminUserEndpoint = adminUserEndpoint;
    }

}
