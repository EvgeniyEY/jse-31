package ru.ermolaev.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.ermolaev.tm.api.ISessionService;
import ru.ermolaev.tm.endpoint.ProjectEndpoint;
import ru.ermolaev.tm.endpoint.SessionDTO;
import ru.ermolaev.tm.util.TerminalUtil;

@Component
public class ProjectRemoveByNameCommand extends AbstractProjectCommand {

    @NotNull
    @Override
    public String commandName() {
        return "project-remove-by-name";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Remove project by name.";
    }

    @Autowired
    public ProjectRemoveByNameCommand(
            @NotNull final ProjectEndpoint projectEndpoint,
            @NotNull final ISessionService sessionService
    ) {
        super(projectEndpoint, sessionService);
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[REMOVE PROJECT]");
        System.out.println("ENTER PROJECT NAME:");
        @Nullable final String name = TerminalUtil.nextLine();
        @Nullable final SessionDTO session = sessionService.getCurrentSession();
        projectEndpoint.removeProjectByName(session, name);
        System.out.println("[COMPLETE]");
    }

}
