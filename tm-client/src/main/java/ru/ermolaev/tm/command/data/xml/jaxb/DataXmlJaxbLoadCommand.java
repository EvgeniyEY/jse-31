package ru.ermolaev.tm.command.data.xml.jaxb;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.ermolaev.tm.api.ISessionService;
import ru.ermolaev.tm.command.data.AbstractDataCommand;
import ru.ermolaev.tm.endpoint.AdminDataEndpoint;
import ru.ermolaev.tm.endpoint.SessionDTO;

@Component
public class DataXmlJaxbLoadCommand extends AbstractDataCommand {

    @NotNull
    @Override
    public String commandName() {
        return "data-xml-jb-load";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Load data from XML (Jax-B) file.";
    }

    @Autowired
    public DataXmlJaxbLoadCommand(
            @NotNull final AdminDataEndpoint adminDataEndpoint,
            @NotNull final ISessionService sessionService
    ) {
        super(adminDataEndpoint, sessionService);
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[DATA XML (JAX-B) LOAD]");
        @Nullable final SessionDTO session = sessionService.getCurrentSession();
        adminDataEndpoint.loadXmlByJaxb(session);
        System.out.println("[OK]");
    }

}
