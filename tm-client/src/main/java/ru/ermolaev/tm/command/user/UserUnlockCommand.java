package ru.ermolaev.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.ermolaev.tm.api.ISessionService;
import ru.ermolaev.tm.endpoint.AdminUserEndpoint;
import ru.ermolaev.tm.endpoint.SessionDTO;
import ru.ermolaev.tm.util.TerminalUtil;

@Component
public class UserUnlockCommand extends AbstractAdminCommand {

    @NotNull
    @Override
    public String commandName() {
        return "user-unlock";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Unlock user.";
    }

    @Autowired
    public UserUnlockCommand(
            @NotNull final AdminUserEndpoint adminUserEndpoint,
            @NotNull final ISessionService sessionService
    ) {
        super(adminUserEndpoint, sessionService);
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[UNLOCK USER]");
        System.out.println("ENTER LOGIN:");
        @Nullable final String login = TerminalUtil.nextLine();
        @Nullable final SessionDTO session = sessionService.getCurrentSession();
        adminUserEndpoint.unlockUserByLogin(session, login);
        System.out.println("[OK]");
    }

}
