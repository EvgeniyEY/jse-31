package ru.ermolaev.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.ermolaev.tm.api.ISessionService;
import ru.ermolaev.tm.endpoint.SessionDTO;
import ru.ermolaev.tm.endpoint.TaskEndpoint;
import ru.ermolaev.tm.util.TerminalUtil;

@Component
public class TaskCreateCommand extends AbstractTaskCommand {

    @NotNull
    @Override
    public String commandName() {
        return "task-create";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Create a new task.";
    }

    @Autowired
    public TaskCreateCommand(
            @NotNull final TaskEndpoint taskEndpoint,
            @NotNull final ISessionService sessionService
    ) {
        super(taskEndpoint, sessionService);
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[CREATE TASK]");
        System.out.println("ENTER TASK NAME:");
        @Nullable final String taskName = TerminalUtil.nextLine();
        System.out.println("ENTER TASK NAME:");
        @Nullable final String projectName = TerminalUtil.nextLine();
        System.out.println("ENTER TASK DESCRIPTION:");
        @Nullable final String description = TerminalUtil.nextLine();
        @Nullable final SessionDTO session = sessionService.getCurrentSession();
        taskEndpoint.createTask(session, taskName, projectName, description);
        System.out.println("[COMPLETE]");
    }

}
