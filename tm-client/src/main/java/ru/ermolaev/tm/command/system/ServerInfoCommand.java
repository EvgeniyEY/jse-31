package ru.ermolaev.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.ermolaev.tm.api.ISessionService;
import ru.ermolaev.tm.command.AbstractCommand;
import ru.ermolaev.tm.endpoint.SessionDTO;
import ru.ermolaev.tm.endpoint.SessionEndpoint;

@Component
public class ServerInfoCommand extends AbstractCommand {

    private final SessionEndpoint sessionEndpoint;

    @NotNull
    @Override
    public String commandName() {
        return "server-info";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Show information about connecting to server.";
    }

    @Autowired
    public ServerInfoCommand(
            @NotNull final SessionEndpoint sessionEndpoint,
            @NotNull final ISessionService sessionService
    ) {
        super(sessionService);
        this.sessionEndpoint = sessionEndpoint;
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[SERVER INFO]");
        @Nullable final SessionDTO session = sessionService.getCurrentSession();
        @NotNull final String host = sessionEndpoint.getServerHost(session);
        @NotNull final Integer port = sessionEndpoint.getServerPort(session);
        System.out.println("HOST: " + host);
        System.out.println("PORT: " + port);
        System.out.println("[OK]");
    }

}
