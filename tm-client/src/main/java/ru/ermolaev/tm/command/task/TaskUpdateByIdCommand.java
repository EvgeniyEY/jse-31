package ru.ermolaev.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.ermolaev.tm.api.ISessionService;
import ru.ermolaev.tm.endpoint.SessionDTO;
import ru.ermolaev.tm.endpoint.TaskEndpoint;
import ru.ermolaev.tm.util.TerminalUtil;

@Component
public class TaskUpdateByIdCommand extends AbstractTaskCommand {

    @NotNull
    @Override
    public String commandName() {
        return "task-update-by-id";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Update task by id.";
    }

    @Autowired
    public TaskUpdateByIdCommand(
            @NotNull final TaskEndpoint taskEndpoint,
            @NotNull final ISessionService sessionService
    ) {
        super(taskEndpoint, sessionService);
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[UPDATE TASK]");
        System.out.println("ENTER TASK ID:");
        @Nullable final String id = TerminalUtil.nextLine();
        System.out.println("ENTER NEW TASK NAME:");
        @Nullable final String name = TerminalUtil.nextLine();
        System.out.println("ENTER NEW TASK DESCRIPTION:");
        @Nullable final String description = TerminalUtil.nextLine();
        @Nullable final SessionDTO session = sessionService.getCurrentSession();
        taskEndpoint.updateTaskById(session, id, name, description);
        System.out.println("[COMPLETE]");
    }

}
