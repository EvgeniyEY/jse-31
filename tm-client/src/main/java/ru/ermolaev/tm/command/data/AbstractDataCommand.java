package ru.ermolaev.tm.command.data;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.ermolaev.tm.api.ISessionService;
import ru.ermolaev.tm.command.AbstractCommand;
import ru.ermolaev.tm.endpoint.AdminDataEndpoint;
import ru.ermolaev.tm.endpoint.ProjectEndpoint;

@Component
public abstract class AbstractDataCommand extends AbstractCommand {

    protected AdminDataEndpoint adminDataEndpoint;

    @Autowired
    public AbstractDataCommand(
            @NotNull final AdminDataEndpoint adminDataEndpoint,
            @NotNull final ISessionService sessionService
    ) {
        super(sessionService);
        this.adminDataEndpoint = adminDataEndpoint;
    }

}
