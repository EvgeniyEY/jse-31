package ru.ermolaev.tm.command;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import ru.ermolaev.tm.api.ISessionService;

public abstract class AbstractCommand {

    protected ISessionService sessionService;

    @Nullable
    public abstract String commandName();

    @Nullable
    public abstract String arg();

    @Nullable
    public abstract String description();

    public AbstractCommand() {
    }

    @Autowired
    public AbstractCommand(@NotNull final ISessionService sessionService) {
        this.sessionService = sessionService;
    }

    public abstract void execute() throws Exception;

}
