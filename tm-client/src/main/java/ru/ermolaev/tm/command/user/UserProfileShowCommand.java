package ru.ermolaev.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.ermolaev.tm.api.ISessionService;
import ru.ermolaev.tm.endpoint.SessionDTO;
import ru.ermolaev.tm.endpoint.UserDTO;
import ru.ermolaev.tm.endpoint.UserEndpoint;

@Component
public class UserProfileShowCommand extends AbstractUserCommand {

    @NotNull
    @Override
    public String commandName() {
        return "user-show-profile";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Show information about user account.";
    }

    @Autowired
    public UserProfileShowCommand(
            @NotNull final UserEndpoint userEndpoint,
            @NotNull final ISessionService sessionService
    ) {
        super(userEndpoint, sessionService);
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[SHOW USER PROFILE]");
        @Nullable final SessionDTO session = sessionService.getCurrentSession();
        @Nullable final UserDTO user = userEndpoint.findUserProfile(session);
        System.out.println("login: " + user.getLogin());
        System.out.println("e-mail: " + user.getEmail());
        System.out.println("first name: " + user.getFirstName());
        System.out.println("middle name: " + user.getMiddleName());
        System.out.println("last name: " + user.getLastName());
        System.out.println("user role: " + user.getRole());
        System.out.println("[OK]");
    }

}
