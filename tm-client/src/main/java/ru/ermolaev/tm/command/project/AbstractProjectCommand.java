package ru.ermolaev.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.ermolaev.tm.api.ISessionService;
import ru.ermolaev.tm.command.AbstractCommand;
import ru.ermolaev.tm.endpoint.ProjectEndpoint;

@Component
public abstract class AbstractProjectCommand extends AbstractCommand {

    protected ProjectEndpoint projectEndpoint;

    @Autowired
    public AbstractProjectCommand(
            @NotNull final ProjectEndpoint projectEndpoint,
            @NotNull final ISessionService sessionService
    ) {
        super(sessionService);
        this.projectEndpoint = projectEndpoint;
    }

}
