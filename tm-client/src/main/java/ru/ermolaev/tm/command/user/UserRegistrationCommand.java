package ru.ermolaev.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.ermolaev.tm.api.ISessionService;
import ru.ermolaev.tm.endpoint.AdminUserEndpoint;
import ru.ermolaev.tm.util.TerminalUtil;

@Component
public class UserRegistrationCommand extends AbstractAdminCommand {

    @NotNull
    @Override
    public String commandName() {
        return "registration";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Registration of new account.";
    }

    @Autowired
    public UserRegistrationCommand(
            @NotNull final AdminUserEndpoint adminUserEndpoint,
            @NotNull final ISessionService sessionService
    ) {
        super(adminUserEndpoint, sessionService);
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[REGISTRATION]");
        System.out.println("ENTER LOGIN:");
        @Nullable final String login = TerminalUtil.nextLine();
        System.out.println("ENTER PASSWORD:");
        @Nullable final String password = TerminalUtil.nextLine();
        System.out.println("ENTER E-MAIL:");
        @Nullable final String email = TerminalUtil.nextLine();
        adminUserEndpoint.createUserWithEmail(login, password, email);
        System.out.println("[OK]");
    }

}
