package ru.ermolaev.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.ermolaev.tm.api.ISessionService;
import ru.ermolaev.tm.command.AbstractCommand;
import ru.ermolaev.tm.endpoint.UserEndpoint;

@Component
public abstract class AbstractUserCommand extends AbstractCommand {

    protected UserEndpoint userEndpoint;

    @Autowired
    public AbstractUserCommand(
            @NotNull final UserEndpoint userEndpoint,
            @NotNull final ISessionService sessionService
    ) {
        super(sessionService);
        this.userEndpoint = userEndpoint;
    }

}
