package ru.ermolaev.tm.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import ru.ermolaev.tm.endpoint.*;

@Configuration
@ComponentScan(basePackages = "ru.ermolaev.tm")
public class ClientConfiguration {

    @Bean
    public SessionEndpointService sessionEndpointService() {
        return new SessionEndpointService();
    }

    @Bean
    public SessionEndpoint sessionEndpoint() {
        return sessionEndpointService().getSessionEndpointPort();
    }

    @Bean
    public AdminDataEndpointService adminDataEndpointService() {
        return new AdminDataEndpointService();
    }

    @Bean
    public AdminDataEndpoint adminDataEndpoint() {
        return adminDataEndpointService().getAdminDataEndpointPort();
    }

    @Bean
    public AdminUserEndpointService adminUserEndpointService() {
        return new AdminUserEndpointService();
    }

    @Bean
    public AdminUserEndpoint adminUserEndpoint() {
        return adminUserEndpointService().getAdminUserEndpointPort();
    }

    @Bean
    public TaskEndpointService taskEndpointService() {
        return new TaskEndpointService();
    }

    @Bean
    public TaskEndpoint taskEndpoint() {
        return taskEndpointService().getTaskEndpointPort();
    }

    @Bean
    public ProjectEndpointService projectEndpointService() {
        return new ProjectEndpointService();
    }

    @Bean
    public ProjectEndpoint projectEndpoint() {
        return projectEndpointService().getProjectEndpointPort();
    }

    @Bean
    public UserEndpointService userEndpointService() {
        return new UserEndpointService();
    }

    @Bean
    public UserEndpoint userEndpoint() {
        return userEndpointService().getUserEndpointPort();
    }

}