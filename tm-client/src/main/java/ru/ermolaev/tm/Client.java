package ru.ermolaev.tm;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.ermolaev.tm.bootstrap.Bootstrap;
import ru.ermolaev.tm.config.ClientConfiguration;

public class Client {

    public static void main(@Nullable final String[] args) {
        @NotNull final AnnotationConfigApplicationContext clientContext =
                new AnnotationConfigApplicationContext(ClientConfiguration.class);
        @NotNull final Bootstrap bootstrap = clientContext.getBean(Bootstrap.class);
        bootstrap.run(args);
    }

}
