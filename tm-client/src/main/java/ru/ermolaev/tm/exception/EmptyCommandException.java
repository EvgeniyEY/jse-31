package ru.ermolaev.tm.exception;

public class EmptyCommandException extends AbstractException {

    public EmptyCommandException() {
        super("Error! Command is empty.");
    }

}
